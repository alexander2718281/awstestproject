package com.example.awstestproject.handlers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class AppExceptionHandler {

    @ExceptionHandler(Exception.class)
    public String defaultErrorHandler(HttpServletRequest request, Exception ex) {
        request.setAttribute("exception", ex);
        request.setAttribute("url", request.getRequestURI());
        return "error";
    }
}
