package com.example.awstestproject.controller;

import com.example.awstestproject.service.IFileService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.net.URLConnection;
import java.util.List;

@Controller
public class FilesController {

    private final IFileService fileService;

    public FilesController(IFileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/bucket/{bucketName}/files")
    public String getBuckets(@PathVariable String bucketName, Model model) {
        List<String> filesNames = fileService.getAllFilesNameInBucket(bucketName);
        model.addAttribute("files", filesNames);
        model.addAttribute("bucket", bucketName);
        return "files";
    }

    @GetMapping(value = "/bucket/{bucketName}/file/{fileName}", produces = {"application/octet-stream"})
    public ResponseEntity getFile(@PathVariable String bucketName, @PathVariable String fileName) {
        HttpHeaders responseHeaders = new HttpHeaders();
        String type = URLConnection.guessContentTypeFromName(fileName);
        responseHeaders.add("content-disposition", "attachment; filename=" + fileName);
        responseHeaders.add("Content-Type",type);

        byte[] file = fileService.downloadFile(bucketName, fileName);

        ResponseEntity responseEntity = new ResponseEntity(file, responseHeaders, HttpStatus.OK);
        return responseEntity;
    }

    @GetMapping("/bucket/{bucketName}/file-upload")
    public String getCreateBucketPage(@PathVariable String bucketName, Model model) {
        model.addAttribute("bucket", bucketName);
        return "file-upload";
    }

    @PostMapping("/bucket/{bucketName}/file")
    public String uploadFile(@PathVariable String bucketName,
                             @ModelAttribute("file") MultipartFile file,
                             Model model) {
        if (file == null || file.isEmpty()) {
            model.addAttribute("bucket", bucketName);
            return "redirect:/bucket/" + bucketName +"/file-upload";
        } else {
            fileService.uploadFile(bucketName, file);
        }
        return "redirect:/bucket/" + bucketName + "/files";
    }

    @GetMapping(value = "/bucket/{bucketName}/file/{fileName}/delete")
    public String deleteFile(@PathVariable String bucketName, @PathVariable String fileName) {
        fileService.deleteFile(bucketName, fileName);
        return "redirect:/bucket/" + bucketName + "/files";
    }
}
