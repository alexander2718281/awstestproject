package com.example.awstestproject.controller;

import com.example.awstestproject.service.IBucketService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class BucketController {

    private final IBucketService bucketService;

    public BucketController(IBucketService bucketService) {
        this.bucketService = bucketService;
    }

    @GetMapping("/buckets")
    public String getBuckets(Model model) {
        List<String> bucketsNames = bucketService.getAllBucketsNames();
        model.addAttribute("buckets", bucketsNames);
        return "buckets";
    }

    @GetMapping("/buckets/{bucketName}/delete")
    public String deleteBucket(@PathVariable("bucketName") String bucketName) {
        bucketService.delete(bucketName);
        return "redirect:/buckets";
    }

    @GetMapping("/bucket-create")
    public String getCreateBucketPage() {
        return "/bucket-create";
    }

    @PostMapping("bucket")
    public String createBucket(String bucket) {
        bucketService.create(bucket);
        return "redirect:/buckets";
    }


}
