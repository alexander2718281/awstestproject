package com.example.awstestproject.service;

import java.util.List;

public interface IBucketService {

    List<String> getAllBucketsNames();

    void delete(String bucketName);

    boolean create(String bucketName);
}
