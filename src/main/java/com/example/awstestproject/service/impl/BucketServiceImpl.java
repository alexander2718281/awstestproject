package com.example.awstestproject.service.impl;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.Bucket;
import com.example.awstestproject.service.IBucketService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

@Service
public class BucketServiceImpl implements IBucketService {

    private final AmazonS3 s3Client;

    public BucketServiceImpl(AmazonS3 s3Client) {
        this.s3Client = s3Client;
    }

    @Override
    public List<String> getAllBucketsNames() {
        List<Bucket> buckets = s3Client.listBuckets();
        return buckets.stream().map(Bucket::getName).collect(Collectors.toList());
    }

    @Override
    public void delete(String bucketName) {
        s3Client.deleteBucket(bucketName);
    }

    @Override
    public boolean create(String bucketName) {
        s3Client.createBucket(bucketName.toLowerCase(Locale.ROOT));
        return true;
    }
}
