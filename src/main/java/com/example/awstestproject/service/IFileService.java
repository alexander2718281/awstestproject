package com.example.awstestproject.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IFileService {

    List<String> getAllFilesNameInBucket(String bucketName);

    byte[] downloadFile(String bucketName, String fileName);

    boolean uploadFile(String bucketName, MultipartFile file);

    void deleteFile(String bucketName, String fileName);
}
