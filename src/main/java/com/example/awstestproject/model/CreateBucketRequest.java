package com.example.awstestproject.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateBucketRequest {

    private String name;    
}
